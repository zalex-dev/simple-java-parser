import com.google.common.collect.ListMultimap;
import com.google.common.collect.Maps;
import com.google.common.collect.MultimapBuilder;
import com.microsoft.playwright.*;

import java.util.Map;

public class Example {
    public static void main(String[] args) {
        try (Playwright playwright = Playwright.create()) {
            Browser browser = playwright.chromium().launch(new BrowserType.LaunchOptions().setHeadless(false));
            Page page = browser.newPage();
            page.navigate("https://www.simbirsoft.com");
            var body = page.innerText("body").toLowerCase();
            var arr = body.replaceAll("[^a-zа-я]", " ").split("[.,\\s]+");

            ListMultimap<String, Integer> treeListMultimap = MultimapBuilder.treeKeys().arrayListValues().build();
            for (String i : arr) {
                treeListMultimap.put(i, 1);
            }

            Map<String, Integer> map = Maps.transformValues(treeListMultimap.asMap(), ints ->
                    ints.stream().mapToInt(Integer::intValue).sum());

            System.out.println(map);

        }

    }

}